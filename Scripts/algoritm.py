#Imported Libaries
from sklearn import datasets
from sklearn.model_selection import train_test_split
from sklearn.tree import DecisionTreeClassifier
from sklearn.metrics import classification_report
import numpy as np
import sys

#Load the dataset using sklearn datasets
iris = datasets.load_iris()
X = iris.data
y = iris.target

#Spliting the data into training and testing
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 0.2, random_state =55)

#DecisionTree algoritm and specific parameters to improve the overall accuracy
classifier = DecisionTreeClassifier(
        criterion = "entropy",
        random_state = 55,
        max_leaf_nodes = 4,
        max_depth = 3
    )

#Fitting the model
classifier.fit(X_train, y_train)
prediciton = classifier.predict(X_test)

#Invoking the matrix.txt and adapting to the model
mat = []
with open(sys.argv[1], 'r') as f:
    for line in f:
        mat.append(list(map(float,line.split())))

#Predicting the model based on the matriz.txt
print(classifier.predict(mat))

#Invoking the Classification Report
print("Classification Report : \n")
print(classification_report(y_test, prediciton) , "\n")
#Predicting the model based on the matriz.txt
print("The predicted values :", "\n")
print(classifier.predict(mat))



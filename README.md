# Machine Learning : Iris Dataset

This project is dedicated to the development of Machine Learning, namely the Decision Tree Classifier, in Iris Dataset.
The dataset, in which is a classification type, is composed of five characteristics: SepalLength, SepalWidth, PetalLength, PetalWidth and class, being the class constituted by three species : Iris Versicolor, Iris Setosa e Iris Virginica.
The purpose of this Machine Learning algorithm is to classify the class of which type of flower species, depending on the values ​​assigned to the characteristics.
The user will have to enter numerical values, in the mentioned characteristics, with the exception of the class, which he wants to assign to obtain the classification of the flower specie. 

# Before Getting Started

The user will have to use matrix.txt as input, introducing the numerical values as type float.
A sample consists of one row and four columns, the columns being with: SepalLength, SepalWidth, PetalLength and PetalWidth, in that order.
It's also able to add more than one sample if the user wishes for it.
Upon completion of the program, the program returns a classification report of the Decision Tree performance, namely the precision, and a list of numbers, depending on the number of samples, between 0 to 2.

* The number 0 corresponds to the species Iris Versicolor.
* The number 1 corresponds to the species Iris Setosa.
* The number 2 corresponds to the species Iris Virginica.

# Requirements

* Docker Installation

# Getting Started

The user will have to have docker installed after that he has to use the following commands :
1) Pull Docker image : `docker pull harthh/andreantunes:lastest`
2) Run Docker : `docker run -v $HOME/Desktop/Input/:/projeto/Input -v $HOME/Desktop/Output/:/projeto/Output -it harthh/andreantunes:latest`
3) Run SnakeFile : `OUTPUTDIR="Output" INPUT="Input/matrix.txt" snakemake --cores all`
* Notes: In step 2 the user has to change the PATH (Input) where the input files are located as well as the PATH (Output) for the output files and in step 4 the "matrix.txt" is the matrix that the user wants to use, having been changed by the responsible for the program.


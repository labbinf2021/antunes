#Image used for the Docker
FROM snakemake/snakemake:main

#The Maintainer of the Dockerfile
LABEL MAINTAINER = 201900177@estudantes.ips.pt

#Creation of the Directories
WORKDIR projeto
COPY Scripts/ /projeto/Scripts/
COPY Input/ /projeto/Input/
COPY Output/ /projeto/Output/
COPY Snakefile /projeto/

#Install python3
RUN apt-get update && \
	apt-get -y install python3-pip

#Install scikit-learn
RUN pip install -U scikit-learn

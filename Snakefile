#Import library
import os

#Setting variables
inputfile = os.environ.get("INPUT")
outputdir = os.environ.get("OUTPUT")

#OUTPUTDIR="Output" INPUT="Input/matrix.txt" snakemake --cores all


singularity: "docker://harthh/andreantunes:latest"

#Create inputs
rule criar:
    input:
        'Input/matrix.txt',
        f'Output/result.txt'

#Remove existence outputs
rule remove:
    run:
        shell('rm -rf Output')

#Run python script and save the output to the correct directory
rule run:
    input:
    	inputfile
    output:
        'Output/result.txt'
    run:
        shell(f'mkdir -p Output')
        shell('python3 Scripts/temp.py {inputfile} > {output}')
